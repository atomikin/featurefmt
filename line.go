package main

import (
	"bufio"
	"log"
	"os"
	"regexp"
)

const (
	feature  = 1 << iota
	tag      = 1 << iota
	scenario = 1 << iota
	when     = 1 << iota
	then     = 1 << iota
	and      = 1 << iota
	given    = 1 << iota
	comment  = 1 << iota
	table    = 1 << iota
	embedded = 1 << iota
	examples = 1 << iota
	any      = 1 << iota
)

var TYPE_REGEX_MAP map[int][]*regexp.Regexp = map[int][]*regexp.Regexp{
	feature: {
		regexp.MustCompile(`^\s{0,}[Ff]eature.*`),
	},
	tag: {
		regexp.MustCompile(`^\s{0,}@.*`),
	},
	when: {
		regexp.MustCompile(`^\s{0,}[Ww]hen.*`), regexp.MustCompile(`^\s{0,}[Кк]огда.*`),
	},
	then: {
		regexp.MustCompile(`^\s{0,}[Tt]hen.*`), regexp.MustCompile(`\s{0,}[Тт]огда.*`),
	},
	given: {
		regexp.MustCompile(`^\s{0,}[Gg]iven.*`), regexp.MustCompile(`^\s{0,}[Дд]опустим.*`),
	},
	and: {
		regexp.MustCompile(`^\s{0,}[Aa]nd.*`), regexp.MustCompile(`^\s{0,}[Ии].*`),
	},
	scenario: {
		regexp.MustCompile(`^\s{0,}[Ss]cenario.*`), regexp.MustCompile(`^\s{0,}[Сс]ценарий.*`),
	},
	table: {
		regexp.MustCompile(`^\s{0,}\|`),
	},
	embedded: {
		regexp.MustCompile(`^\s{0,}"""\s{0,}`),
	},
	comment: {
		regexp.MustCompile(`^\s{0,}#\s{0,}`),
	},
	examples: {
		regexp.MustCompile(`^\s{0,}[Ee]xamples:.*`), regexp.MustCompile(`^\s{0,}[Пп]римеры:.*`),
	},
}

// var OFFSET map[int]int = map[int][int] {
// 	feature: 0,
// 	scenario: 2,
// }

type Line struct {
	Key  int
	Data string
	File string
	No   int
}

type Container interface {
	Head() *Line
	Tail() *Line
}

type Lines []*Line

func (self Lines) Head() *Line {
	if len(self) == 0 {
		return nil
	}
	return self[0]
}

func (self Lines) Tail() *Line {
	if len(self) == 0 {
		return nil
	}
	return self[len(self)-1]
}

func ResolveKey(rawLine string) int {
	for key, value := range TYPE_REGEX_MAP {
		for _, reg := range value {
			if reg.MatchString(rawLine) {
				return key
			}
		}
	}
	return any
}

func ToLines(fileName string) Lines {
	file, err := os.Open(fileName)
	if err != nil {
		log.Fatal(err)
	}
	scanner := bufio.NewScanner(file)
	lines := make(Lines, 0, 20)
	lineNum := 0
	for scanner.Scan() {
		text := scanner.Text()
		lines = append(lines, &Line{Data: text, No: lineNum, File: fileName, Key: ResolveKey(text)})
		lineNum++
	}
	return lines
}
