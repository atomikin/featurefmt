package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
)

func format(lines Lines, rightMargin int) Lines {
	// init containers
	body := make(Lines, 0, 20)
	embeddedText := make(Lines, 0, 20)
	tableLines := make(Lines, 0, 20)

	for _, line := range lines {

		switch line.Key {
		case table:
			if len(embeddedText) > 0 {
				embeddedText = append(embeddedText, line)
			} else {
				tableLines = append(tableLines, line)
			}
		case embedded:
			if len(tableLines) > 0 {
				l := FormatTable(tableLines, MarginRight(body.Tail().Key, rightMargin))
				body = append(body, &l)
				tableLines = make(Lines, 0, 20)
			}
			embeddedText = append(embeddedText, CleanLine(line, MarginRight(body.Tail().Key, rightMargin)))
			if len(embeddedText) > 1 {
				l := FormatEmbeddedText(embeddedText, MarginRight(body.Tail().Key, rightMargin))
				body = append(body, &l)
				embeddedText = make(Lines, 0, 20)
			}
		default:
			if len(tableLines) > 0 {
				l := FormatTable(tableLines, MarginRight(body.Tail().Key, rightMargin))
				body = append(body, &l)
				tableLines = make(Lines, 0, 20)
			}
			if len(embeddedText) > 0 {
				embeddedText = append(embeddedText, line)
			} else {
				data := CleanLine(line, rightMargin)
				if data == nil {
					continue
				}
				body = append(body, data)
			}
		}
	}
	if len(tableLines) > 0 {
		l := FormatTable(tableLines, MarginRight(body.Tail().Key, rightMargin))
		body = append(body, &l)
	}
	return body
}

type CliArgs struct {
	File  string
	Write bool
}

func (self *CliArgs) ParseArgs() *CliArgs {
	flag.StringVar(&self.File, "file", ".", "location of feature files")
	flag.BoolVar(&self.Write, "write", false, "Write to the original file")
	flag.Parse()
	return self
}

func main() {
	args := new(CliArgs).ParseArgs()
	lines := ToLines(args.File)
	result := format(lines, 0)
	var writer *bufio.Writer
	if args.Write {
		file, err := os.Create(args.File)
		if err != nil {
			panic(err)
		}
		writer = bufio.NewWriter(file)
	} else {
		writer = bufio.NewWriter(os.Stdout)
	}
	for _, i := range result {
		fmt.Fprintln(writer, i.Data)
	}
	writer.Flush()
}
