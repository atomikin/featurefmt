package main

import (
	"fmt"
	"testing"
)

func TestResolveType(t *testing.T) {
	data1 := "When I see"
	data2 := "Тогда я делаю что-то"
	data3 := "Cuando hago algo"

	key := ResolveKey(data1)
	if key != when {
		t.Fail()
	}
	key = ResolveKey(data2)
	if key != then {
		t.Fail()
	}
	key = ResolveKey(data3)
	if key != any {
		t.Fail()
	}
}

func TestReadLines(t *testing.T) {
	lines := ToLines(`../test.feature`)
	if len(lines) == 0 {
		t.Fail()
	}
	if lines.Head().No != 0 {
		t.Errorf("No should be 0, got %d", lines.Head().No)
	}
	if lines.Tail().No != 30 {
		t.Errorf("No should be 30, got %d", lines.Tail().No)
	}
}

func TestFormat(t *testing.T) {
	lines := ToLines(`../test.feature`)
	lines = format(lines, 0)
	for _, line := range lines {
		fmt.Println(line.Data)
	}
}
