package main

import (
	"errors"
	"regexp"
	"strings"
)

var SPACES *regexp.Regexp = regexp.MustCompile(`\s+`)
var FRONT_SPACES *regexp.Regexp = regexp.MustCompile(`^\s{0,}`)
var BACK_SPACES *regexp.Regexp = regexp.MustCompile(`\s{0,}$`)

func FailOnError(err error) {
	if err != nil {
		panic(err)
	}
}

// bulk data formatters

func MarginRight(kind int, offset int) int {
	result := 0
	switch kind {
	case feature:
		result = 0
	case scenario, examples, embedded, table, tag:
		result = 2
	default:
		result = 4
	}
	return result + offset
}

func MarginTop(kind int) int {
	switch kind {
	case when, given, scenario, examples:
		return 1
	case tag:
		return 2
	default:
		return 0
	}
}

func Strip(line *Line) *Line {
	line.Data = FRONT_SPACES.ReplaceAllString(line.Data, "")
	line.Data = BACK_SPACES.ReplaceAllString(line.Data, "")
	return line
}

func CleanLine(line *Line, offset int) *Line {
	line = Strip(line)
	line.Data = SPACES.ReplaceAllString(line.Data, " ")

	if line.Data != "" {
		line.Data = strings.Repeat("\n", MarginTop(line.Key)) + strings.Repeat(" ", MarginRight(line.Key, offset)) + line.Data
		return line
	}
	return nil
}

func FormatTable(tableLines Lines, offset int) Line {
	var maxVals []int
	splitted := make([][]string, 0, 5)
	for i, line := range tableLines {
		s, err := splitTableLine(line)
		FailOnError(err)
		if i == 0 {
			maxVals = make([]int, len(s), len(s))
		}
		for i, elem := range s {
			possibleMax := len(elem)
			if possibleMax > maxVals[i] {
				maxVals[i] = possibleMax
			}
		}
		splitted = append(splitted, s)
	}
	result := make([]string, 0, 10)
	for _, l := range splitted {
		for i, elem := range l {
			l[i] = elem + strings.Repeat(" ", maxVals[i]-len(elem))
		}
		result = append(result, strings.Repeat(" ", MarginRight(table, offset))+"| "+strings.Join(l, " | ")+" |")
	}
	newLine := Line{Data: strings.Join(result, "\n"), No: tableLines.Head().No, Key: tableLines.Head().Key, File: tableLines.Head().File}
	return newLine
}

func splitTableLine(line *Line) ([]string, error) {
	regex := regexp.MustCompile(`\|`)
	data := regex.Split(Strip(line).Data, -1)
	if len(data[0]) == 0 && len(data[len(data)-1]) == 0 {
		data = data[1 : len(data)-1]
		for i, elem := range data {
			d := FRONT_SPACES.ReplaceAllString(elem, "")
			d = BACK_SPACES.ReplaceAllString(d, "")
			data[i] = d
		}
		return data, nil
	}
	err := errors.New("Malformed table: \"" + line.Data + "\"")
	return nil, err
}

func FormatEmbeddedText(text Lines, offset int) Line {
	result := make([]string, 0, 20)
	result = append(result, text.Head().Data)
	if len(text) > 2 {
		for _, i := range format(text[1:len(text)-1], offset) {
			result = append(result, i.Data)
		}
	}
	result = append(result, text.Tail().Data)
	newLine := Line{Data: strings.Join(result, "\n"), Key: text.Head().Key, File: text.Head().File, No: text.Head().No}
	return newLine
}
